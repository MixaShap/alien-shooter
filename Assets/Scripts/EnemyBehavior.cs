﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour {

	private Rigidbody2D body;

	private AudioSource explode_sound;


    // Start is called before the first frame update
    void Start() {
		body = GetComponent<Rigidbody2D>();
		explode_sound = GameObject.FindObjectOfType<AudioSource>();
    }


    void FixedUpdate() {

    }


    // Update is called once per frame
    void Update() {

    }


    void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("OnTriggerEnter2D Enemy");
        // explode_sound.Play();
        // sometimes object dont want to die
    	Destroy(gameObject);
    }
}
