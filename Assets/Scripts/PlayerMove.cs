﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float speed = 10;
	public KeyCode leftButton = KeyCode.A;
	public KeyCode rightButton = KeyCode.D;
	public KeyCode upButton = KeyCode.W;
	public KeyCode downButton = KeyCode.S;
	public KeyCode fireButton = KeyCode.Space;
	public bool isGlide = true;
	public float glide = 100f;
	public GameObject Laser;
	public int shoots_max = 5;
	public float delay_shoot_s = 0.1f;

	private Vector3 direction;
	private float horizontal;
	private Rigidbody2D body;
	private int shoots_amount = 0;
	private Camera cam;
	private float height;
	private float shoot_timer = 1;			// timer for shoot interval


	void Start() {
		body = GetComponent<Rigidbody2D>();
		cam = Camera.main;
		height = cam.orthographicSize;
		Debug.Log("H = " + height.ToString());
	}


	void FixedUpdate() {
		if ((body.transform.position.x < height * 1.6) && (horizontal != -1) || (body.transform.position.x > -height * 1.6) && (horizontal != 1)) {
			body.AddForce(direction * body.mass * speed);
			if (Mathf.Abs(body.velocity.x) > speed / glide) {
				body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * speed / glide, body.velocity.y);
			}
		} else {
			body.velocity = new Vector2(0, body.velocity.y);
		}

		if (isGlide == false) {
			body.velocity = new Vector2(0, body.velocity.y);	
		}
	}


	void Update() {
		shoot_timer += Time.deltaTime;	

		if (Input.GetKey(leftButton)) 
			horizontal = -1;
		else if (Input.GetKey(rightButton)) 
			horizontal = 1; 
		else 
			horizontal = 0;

		shoots_amount = GameObject.FindGameObjectsWithTag("Laser").Length;
		if ((Input.GetKey(fireButton)) && (shoots_amount < shoots_max) && (shoot_timer > delay_shoot_s)) {
			Instantiate(Laser, new Vector3(body.transform.position.x, body.transform.position.y, 0.1f), Quaternion.identity);
			shoot_timer = 0;
		}

		direction = new Vector2(horizontal, 0);
	}
}
