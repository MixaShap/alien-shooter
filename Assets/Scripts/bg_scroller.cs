﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class bg_scroller : MonoBehaviour {

    public float scrollSpeed = 10;

    private Renderer renderer_bg;
    private Vector2 savedOffset;


    void Start () {
        renderer_bg = GetComponent<Renderer>();
    }


    void Update () {
		float y = Mathf.Repeat(Time.time * scrollSpeed, 1);
		Vector2 offset = new Vector2 (0, y);
		renderer_bg.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

}