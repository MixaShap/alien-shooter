﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCollision : MonoBehaviour {

	private Rigidbody2D body;
    private Camera cam;
    private float height;
    private float width;

    // Start is called before the first frame update
    void Start() {
		body = GetComponent<Rigidbody2D>();
        cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
    }

    // Update is called once per frame
    void Update() {
        if (body.transform.position.y > width) {
        	Destroy(gameObject);
        }
    }


    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Enemy") {
            Debug.Log("OnTriggerEnter2D Laser");
            Destroy(gameObject);
        }
    }
}
