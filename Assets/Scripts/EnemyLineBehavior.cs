﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLineBehavior : MonoBehaviour {

	public float speed = 1000f;
	public GameObject enemy_array;

	private GameObject[] enemies;
	private Rigidbody2D body;
	private Vector2 direction;		
	private float newDirectionX;
	private int downDistCounter = 0;
	private Camera cam;
    private float height;
    private float width;
    private int enemy_array_spawned = 0;

	// Start is called before the first frame update
	void Start() {
		direction = new Vector2(-1, 0);		// left

		body = GetComponent<Rigidbody2D>();
		body.AddForce(direction * body.mass * speed);		// left

		cam = Camera.main;
        height = 2f * cam.orthographicSize;
        width = height * cam.aspect;
	}


	void FixedUpdate() {
		if (((Mathf.Abs(body.transform.position.x) > width / 13) && (direction.x != 0)) || ((direction.x == 0) && (downDistCounter == 60))){
			goDown();
			downDistCounter = 0;
		} else if (direction.x == 0) {
			downDistCounter++;
		}
	}


	// Update is called once per frame
	void Update() {
		// call new line here after 50px down
	}


	void goDown() {
		if (direction.x == -1) {
			body.AddForce(new Vector2(1, -1) * body.mass * speed);		// down from left
			newDirectionX = -direction.x;
			direction.x = 0;
			direction.y = -1;
		} else if (direction.x == 1) {
    		body.AddForce(new Vector2(-1, -1) * body.mass * speed);		// down from right
			newDirectionX = -direction.x;
			direction.x = 0;
			direction.y = -1;
		} else if (direction.x == 0) {
			body.AddForce(new Vector2(newDirectionX, 1) * body.mass * speed);		// stop down + to the other side
			direction.x = newDirectionX;
			direction.y = 0;

			// bug spawns less then 5 subjects
			if (enemy_array_spawned == 0) {
				Instantiate(enemy_array, new Vector3(25, 40, 0.5f), Quaternion.identity);
				enemy_array_spawned = 1;
			}
		}
	}
}
